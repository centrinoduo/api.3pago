<?php defined('BASEPATH') OR exit('No direct script access allowed');




class Tbl_users_profile extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }


private $profile_table_name = 'tbl_users_profile';  // user profiles
private $users_table = 'tbl_users';  // user profiles



/**
 * Shows the profile identifier.
 *
 * @param      <type>  $data   The data
 *
 * @return     <type>  ( description_of_the_return_value )
 */
 public function show_profile_id($data){
$this->db->select('*');
$this->db->from($this->user_profiles); // table 
$this->db->where('user_id', $data);  //
$query = $this->db->get();
$result = $query->result();
return $result;
}


/**
 * update data user.
 *
 * @param      <type>  $id     The identifier
 * @param      <type>  $data   The data
 */
 public function save_data($id,$data){
$this->db->where('user_id', $id);
$this->db->update($this->user_profiles, $data);
}




/**
 * Gets the data user by email.
 *
 * @param      <type>  $email  The email
 *
 * @return     <type>  The data user by email.
 */
function get_data_user_by_email($email)
{


    $this->db->select('*');
    $this->db->from('user_profiles');
    $this->db->where('LOWER(email)=', strtolower($email));
    $query = $this->db->get();
     if ($query->num_rows() == 1) return $query->row();
        return NULL;

}




/**
 * { Check user by email }
 *
 * @param      <type>  $email  The email
 *
 * @return     <type>  ( description_of_the_return_value )
 */
function check_user_by_mail($email)
{

    $this->db->select('*');
    $this->db->from('user_profiles');
    $this->db->where('email',$email);
    $this->db->where('LOWER(email)=', strtolower($email));
    $query = $this->db->get();
    if ($query->num_rows() == 1) return $query->row();
        return NULL;

}




    /**
     * get_user_id_from_username function.
     * 
     * @access public
     * @param mixed $username
     * @return int the user id
     */
    public function get_user_id_from_email($email) {
        
        $this->db->select('id');
        $this->db->from($this->users_table);
        $this->db->where('email', $email);
        return $this->db->get()->row('id');
        
    }
    







   function getEmployees($limit, $start) {
        $query = $this->db->get('Employees', $limit, $start);
        return $query->result();
    }





/**
 * Gets the user profile.
 *
 * @param      <type>  $id_user  The identifier user
 *
 * @return     <type>  The user profile.
 */
public function get_user_profile($id_user) 

    {

        $this->db->select('name,last_name,phone_verified,phone_number,email,addres,postal_code,identification_number');
        $this->db->from($this->users_table);
        $this->db->where('id', $id_user);
       $query = $this->db->get();

        return    $query->result();


        
    }


/**
 * [updateuser description]
 * @param  [type] $id_user   [description]
 * @param  [type] $name      [description]
 * @param  [type] $last_name [description]
 * @return [type]            [description]
 */
public function update_user($datauser)

{  

$this->db->where('id', $datauser['id']);
$update = $this->db->update($this->users_table, $datauser);

if($update)
    
   return true;
else
    return false;

 }




 /**
     * get_user function.
     * 
     * @access public
     * @param mixed $user_id
     * @return object the user object
     */
    public function get_user($user_id) {
        

        $this->db->select('name,last_name,email,username');
        $this->db->from($this->users_table);
        $this->db->where('id', $user_id); 
        $query = $this->db->get();

        # $query = $this->db->get()->row();

        return    $query->result();

         
        
    }


/**
 * Gets the user.
 *
 * @param      <type>  $user_id  The user identifier
 *
 * @return     <type>  The user.
 */
public function getUser($user_id)
{
    $this->db->select('name,last_name,email,username,phone_number,phone_verified');
       # $this->db->from('users');
            $this->db->where('id', $user_id); 
    #$this->db->select('name,last_name,email,username');
    $query = $this->db->get($this->users_table);
    return $query->row(); //row() devuelve la primer fila
}



   


  /**
 * [get number user ]
 * @param  [type] $str [int]
 * @return [type]      [description]
 */
function get_user_phone($str)
{

        $this->db->select('username');
        $this->db->from($this->users_table);
        $this->db->where('id', $str);
        $query = $this->db->get();

 if ($query->num_rows() == 1) return $query->row('username');
        return NULL;

}




  /**
 * [get number user ]
 * @param  [type] $str [int]
 * @return [type]      [description]
 */
function get_user_hex_color($str)
{

        $this->db->select('hex_color');
        $this->db->from($this->users_table);
        $this->db->where('id', $str);
        $query = $this->db->get();

 if ($query->num_rows() == 1) return $query->row('hex_color');
        return NULL;

}



/**
 * [getUserFullname ]
 * @param  [type] $id_user [description]
 * @return [type]          [description]
 */
public function get_user_fullname($id_user){

  #$this->db->from('founds');
$this->db->where('id', $id_user);
$this->db->select('name, last_name');

$query = $this->db->get($this->users_table);


foreach ($query->result() as $row)
{          
        return ucwords($row->name)." ". ucwords($row->last_name) ;
       
}


 }




public function get_email_user($str)
{

        $this->db->select('email');
        $this->db->from($this->users_table);
        $this->db->where('id', $str);
        $query = $this->db->get();

 if ($query->num_rows() == 1) return $query->row('email');
        return NULL;

}





/**
 * [get Fullname user ]
 * @param  [type] $id_user [description]
 * @return [type]          [description]
 */
public function get_user_data($str){

  #$this->db->from('founds');
#$this->db->from('users');
$this->db->where('id', $str);
$this->db->select('id,name,last_name,username,email');

$query = $this->db->get($this->users_table);

        if($query->num_rows()>0){
        return $query->result();
      }

 }









}

