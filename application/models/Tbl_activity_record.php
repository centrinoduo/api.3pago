<?php   defined('BASEPATH') OR exit('No direct script access allowed');




class Tbl_activity_record extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }




   /*Table name*/
    private $table_name = 'tbl_activity_record';


    /**
     * get session recordid function.
     * 
     * @access public
     * @param mixed $username
     * @return int the user id
     */
    public function get_session_record ($id_user) 

    {
        
 #$query = $this->db->get('mytable', 10, 20);

        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->order_by('date', 'DESC');
        $this->db->where('id_user', $id_user);
        
        $query = $this->db->get();

        return    $query->result_array();

        
        #return    $query1;
        
    }
    


/**
     * get user found by id function.
     * 
     * @access public
     * @param mixed $username
     * @return int the user id
     */
    public function get_session_recordx3 ($id_user) 

    {
        
 #$query = $this->db->get('mytable', 10, 20);

        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->order_by('date', 'DESC');
        $this->db->where('id_user', $id_user);
        $this->db->limit(3);
        
        $query = $this->db->get();

        return    $query->result_array();


        
    }
    



/**
 * [save_record description]
 * @param  [type] $ip      [description]
 * @param  [type] $agent   [description]
 * @param  [type] $user_id [description]
 * @return [type]          [description]
 */
 public function save_record($data) 

    {
        

$this->db->insert($this->table_name, $data);

 return $this->db->insert_id();

        
    }




   
    }/*end class*/
