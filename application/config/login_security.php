<?php
/**
 * CodeIgniter Log config
 *
 * @category   Applications
 * @package    CodeIgniter
 * @subpackage Config
 * @author     Bo-Yi Wu <appleboy.tw@gmail.com>
 * @license    BSD License
 * @link       http://blog.wu-boy.com/
 * @since      Version 1.0
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Login settings
|
| 'login_by_username' = Username can be used to login.
| 'login_by_email' = Email can be used to login.
| You have to set at least one of 2 settings above to TRUE.
| 'login_by_username' makes sense only when 'use_username' is TRUE.
|
| 'login_record_ip' = Save in database user IP address on user login.
| 'login_record_time' = Save in database current time on user login.
|
| 'login_count_attempts' = Count failed login attempts.
| 'login_max_attempts' = Number of failed login attempts before CAPTCHA will be shown.
| 'login_attempt_expire' = Time to live for every attempt to login. Default is 24 hours (60*60*24).
|--------------------------------------------------------------------------
*/




/*
|--------------------------------------------------------------------------
| BASIC SETTING LOGIN SECURED
|--------------------------------------------------------------------------
*/


// Show history login in account
$config['show_history_login_in_home'] = FALSE;

// save_history_login
$config['save_history_login'] = TRUE;






/*
|--------------------------------------------------------------------------
| ADVANCE LOGIN SECURED
|--------------------------------------------------------------------------
*/

$config['ADVANCE_LOGIN_SECURE'] = FALSE;


// count login attemp by id user
$config['login_count_attempts_security'] = FALSE;


// login_max_attempts
$config['login_max_attempts'] = 5;


// login record attempts
$config['login_count_attempts'] = FALSE;
// block account if attempts excceded
$config['block_count_attempts'] = FALSE;


// email_notify_login_max_attempts
$config['block_account_max_attempts_'] = FALSE;
// email_notify_login_max_attempts
$config['email_notify_login_max_attempts_'] = FALSE;