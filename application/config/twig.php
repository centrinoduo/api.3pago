<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
  Twig config
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|
*/


$config['paths'] = '/application/templates/twig/';
$config['cache'] = '/application/cache/twig/';



	#'paths' => ['/path/to/twig/templates', VIEWPATH],
	#'cache' => '/path/to/twig/cache',


/* End of file Twig.php */
/* Location: ./application/config/Twig.php */